var JSON_data;
var recipe_list;
var recipe_tags;

function get_data_array(data){
    
    var data_array = [];
    var all_tags = new Set([]);
   
    var headers = [];
    for (let i = 0; i<data.values[0].length; i++){
       headers.push(data.values[0][i]);
    }

    for (let i = 1; i < data.values.length; i++){
        let datum = Object();
        for (let j=0; j <data.values[0].length; j++){
			if (headers[j] === "TAGS"){
				datum[headers[j]] = new Set(data.values[i][j].split(/, /));
			}else{
				datum[headers[j]] = data.values[i][j];
			}
        }
		all_tags = new Set([...all_tags, ...datum.TAGS]);
        data_array.push(datum);
    }
	return {tags: all_tags, 
			data_array: data_array};
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 * from https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function recipe_date_compare(r1,r2) {
  let a = new Date(r1.COOKED_ON);
  let b = new Date(r2.COOKED_ON);
  if (a < b)
    return 1;
  if (a > b)
    return -1;
  return 0;
}

function recipe_peter_compare(r1,r2) {
  if (r1.PETER_RATING < r2.PETER_RATING)
    return 1;
  if (r1.PETER_RATING > r2.PETER_RATING)
    return -1;
  return 0;
}

function recipe_caitlin_compare(r1,r2) {
  if (r1.CAITLIN_RATING < r2.CAITLIN_RATING)
    return 1;
  if (r1.CAITLIN_RATING > r2.CAITLIN_RATING)
    return -1;
  return 0;
}

function draw_filters(tags){

	let filter_div = document.getElementById("filtertags");
	filter_div.innerHTML=""; 

	let a_tags = [...tags].sort();
	
	for (let item of a_tags){
		
		label = document.createElement("label");
		label.className = "btn btn-secondary";
		label.append(item);		
		label.data = item;
		filter_div.appendChild(label);
	}

}

(function ($) {
  $(document).ready(function(){

	function draw_table(recipe_array){ 
		let main_div = document.getElementById("recipepanel");
		main_div.innerHTML=""; 
        
        let table = document.createElement("table");
        table.className = "table table-hover table-sm"

        let table_head = document.createElement("thead");
        let head_row = document.createElement("tr");
        table.appendChild(table_head);
        table_head.appendChild(head_row);

	    for (let item of ["Recipe", "Last Cooked", "Peter Rating", "Caitlin Rating"]){
            let head_cel = document.createElement("th");
            head_cel.scope = "col";
            head_cel.innerHTML = item;
            head_row.appendChild(head_cel); 
        }
        
        let tbody = document.createElement("tbody");
        table.appendChild(tbody);

        for (let i = 0; i < recipe_array.length; i++){
		    currdata = recipe_array[i];

            let row = document.createElement("tr");
            tbody.appendChild(row);
           
            let cel = document.createElement("th");
            cel.scope = "col";
            let href_text = document.createElement("a");
			href_text.href=currdata.LINK_URL;
			href_text.innerHTML=currdata.NAME;;
			cel.appendChild(href_text);

            row.appendChild(cel); 

            for (let item of ["COOKED_ON", "PETER_RATING", "CAITLIN_RATING"]){
                let cel = document.createElement("td");
                cel.scope = "col";
                cel.innerHTML = currdata[item];
                row.appendChild(cel); 
            }

        }

        main_div.appendChild(table);
        

    }

	function draw_cards(recipe_array){ 
		let main_div = document.getElementById("recipepanel");
		main_div.innerHTML=""; 
        let row_div = document.createElement("div");
        row_div.className="row";
        main_div.appendChild(row_div);

		for (let i = 0; i < recipe_array.length; i++){
			
			let currdata = recipe_array[i];

			let divcol = document.createElement("div");
			divcol.className = "col-md-3";
            //main_div.appendChild(divcol);
			row_div.appendChild(divcol);

			let divcard = document.createElement("div");
			divcard.className = "card mb-3";
			divcol.appendChild(divcard);
			
			let img = document.createElement("img");
			img.className="card-img-top"; 
			img.src=currdata.IMG_URL;
			
			img.setAttribute("data-toggle","popover");
			let tag_array = [...currdata.TAGS];
			img.setAttribute("title",tag_array.toString());
			img.setAttribute("data-html","true");
			img.setAttribute("data-content","Peter: "+ currdata.PETER_RATING +" Caitlin: "+ currdata.CAITLIN_RATING + "<br />last cooked: " + currdata.COOKED_ON);
			
			divcard.append(img); 

			let divcardbody = document.createElement("div");
			divcardbody.className = "card-body";
			divcard.appendChild(divcardbody);
			
			let href_text = document.createElement("a");
			href_text.href=currdata.LINK_URL;

			let text = document.createElement("p");
			text.className="card-text";
			text.innerHTML=currdata.NAME;
			
			href_text.appendChild(text);
			divcardbody.append(href_text);
			
		}
		
		$(function () {
		  $('[data-toggle="popover"]').popover()
		})
	}
    
    //

    //$.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1VvaXyDbPiswnC1V49Y92jM4XcpDsJtQkWSGsmZ-82Sk/values/Sheet1?key=AIzaSyAfRXwQu3DM67Nh5GZm6-lD1W-bq79hvIQ', function(data)   {
    $.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1VvaXyDbPiswnC1V49Y92jM4XcpDsJtQkWSGsmZ-82Sk/values/cooking?key=AIzaSyAfRXwQu3DM67Nh5GZm6-lD1W-bq79hvIQ', function(data)   {
        recipe_data = get_data_array(data);
        recipe_list = recipe_data.data_array;
        recipe_tags = recipe_data.tags;
        draw_cards(recipe_list.sort(recipe_date_compare));
 		draw_filters(recipe_tags);
    });
	
    $('#SortPeter').on('click', function(){
 		draw_filters(recipe_tags);
        if ($('#option_list').hasClass("active")){
            draw_table(recipe_list.sort(recipe_peter_compare));
        }else{
            draw_cards(recipe_list.sort(recipe_peter_compare));
        }
    });
    
    $('#SortCaitlin').on('click', function(){
 		draw_filters(recipe_tags);
        if ($('#option_list').hasClass("active")){
            draw_table(recipe_list.sort(recipe_caitlin_compare));
        }else{
            draw_cards(recipe_list.sort(recipe_caitlin_compare));
        }
    });

    $('#Randomize').on('click', function(){

 		draw_filters(recipe_tags);
        if ($('#option_list').hasClass("active")){
            draw_table(shuffle(recipe_list));
        }else{
            draw_cards(shuffle(recipe_list));
        }

    });
    
	$('#SortCooked').on('click', function(){
 		draw_filters(recipe_tags);
        
        if ($('#option_list').hasClass("active")){
            draw_table(recipe_list.sort(recipe_date_compare));
        }else{
            draw_cards(recipe_list.sort(recipe_date_compare));
        }
    });
	
    $('#option_cooking').on('click', function(){

        $.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1VvaXyDbPiswnC1V49Y92jM4XcpDsJtQkWSGsmZ-82Sk/values/cooking?key=AIzaSyAfRXwQu3DM67Nh5GZm6-lD1W-bq79hvIQ', function(data)   {
            recipe_data = get_data_array(data);
            recipe_list = recipe_data.data_array;
            recipe_tags = recipe_data.tags;
            draw_filters(recipe_tags);
            if ($('#option_list').hasClass("active")){
                draw_table(recipe_list.sort(recipe_date_compare));
            }else{
                draw_cards(recipe_list.sort(recipe_date_compare));
            }
        });
    });

    $('#option_baking').on('click', function(){

        //$.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1LowTJwM86UAI5bWUsxFK2GGN-VDLTBGvP98c07BF2xM/values/Sheet1?key=AIzaSyAfRXwQu3DM67Nh5GZm6-lD1W-bq79hvIQ', function(data)   {
        $.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1VvaXyDbPiswnC1V49Y92jM4XcpDsJtQkWSGsmZ-82Sk/values/baking?key=AIzaSyAfRXwQu3DM67Nh5GZm6-lD1W-bq79hvIQ', function(data)   {
            recipe_data = get_data_array(data);
            recipe_list = recipe_data.data_array;
            recipe_tags = recipe_data.tags;
            draw_filters(recipe_tags);
            if ($('#option_list').hasClass("active")){
                draw_table(recipe_list.sort(recipe_date_compare));
            }else{
                draw_cards(recipe_list.sort(recipe_date_compare));
            }
        });
    });


    $('#option_tocook').on('click', function(){
        $.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1VvaXyDbPiswnC1V49Y92jM4XcpDsJtQkWSGsmZ-82Sk/values/tocook?key=AIzaSyAfRXwQu3DM67Nh5GZm6-lD1W-bq79hvIQ', function(data)   {
            recipe_data = get_data_array(data);
            recipe_list = recipe_data.data_array;
            recipe_tags = recipe_data.tags;
            draw_filters(recipe_tags);
            if ($('#option_list').hasClass("active")){
                draw_table(recipe_list.sort(recipe_date_compare));
            }else{
                draw_cards(recipe_list.sort(recipe_date_compare));
            }
        });
    });

    $('#option_tobake').on('click', function(){

        $.getJSON('https://sheets.googleapis.com/v4/spreadsheets/1VvaXyDbPiswnC1V49Y92jM4XcpDsJtQkWSGsmZ-82Sk/values/tobake?key=AIzaSyAfRXwQu3DM67Nh5GZm6-lD1W-bq79hvIQ', function(data)   {
            recipe_data = get_data_array(data);
            recipe_list = recipe_data.data_array;
            recipe_tags = recipe_data.tags;
            draw_filters(recipe_tags);
            if ($('#option_list').hasClass("active")){
                draw_table(recipe_list.sort(recipe_date_compare));
            }else{
                draw_cards(recipe_list.sort(recipe_date_compare));
            }
        });
    });




    $('#option_list').on('click', function(){
 		draw_filters(recipe_tags);
	    draw_table(recipe_list); 
    });

    $('#option_image').on('click', function(){
 		draw_filters(recipe_tags);
        draw_cards(recipe_list);
    });
	
	
	$('#filtertags').on('click', function(clicked){
		
		let activated_tags = [];		
		$('#filtertags .active').each(function(i, value){ 
			activated_tags.push(value.data);		
		});
		
		if (clicked.target.classList.contains("active")){
			var index = activated_tags.indexOf(clicked.target.data);
			if (index !== -1) activated_tags.splice(index, 1);
		}else{
			activated_tags.push(clicked.target.data);		
		}
		
		var sub_list = [];
		for (let i =0; i<recipe_list.length; i++){
			let datum = recipe_list[i];	
			let intersect = new Set([...activated_tags].filter(x => datum.TAGS.has(x)));
			if (intersect.size === activated_tags.length){
				sub_list.push(datum);	
			}
		}
		
        if ($('#option_list').hasClass("active")){
            draw_table(sub_list);
        }else{
            draw_cards(sub_list);
        }
    });
   	
  });
})(jQuery);


